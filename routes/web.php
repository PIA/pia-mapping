<?php

use Illuminate\Support\Facades\Route;

use App\Models\Document;
use App\Models\Map;
use App\Models\MapEntry;
use App\Models\Place;

use App\Http\Controllers\MapsController;
use App\Http\Controllers\MapLayersController;
use App\Http\Controllers\MapKeysController;
use App\Http\Controllers\MapEntriesController;

use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend/home', [
        'maps_mapping' => Map::where('origin', '=', 'mapping')->orWhere('origin', null)->get(),
        'maps_collection' => Map::where('origin', '=', 'collection')->get(),
    ]);
});

Route::get('/maps/{id}/export', [MapsController::class, 'export'])->name('maps.export');
Route::get('/maps/{id}/images', [MapsController::class, 'images'])->name('maps.images');
Route::post('/maps/{id}/link', [MapsController::class, 'link'])->name('maps.link');
Route::post('/maps/copy', [MapsController::class, 'copy'])->name('maps.copy');
Route::patch('/maps/{id}/imagesUpdate', [MapsController::class, 'imagesUpdate'])->name('maps.imagesUpdate');

/* import */
Route::post('/map-entries/import', [MapEntriesController::class, 'import'])->name('map_entries.import');
Route::post('/map-keys/import', [MapKeysController::class, 'import'])->name('map_keys.import');

/* batch edit */
Route::get('/map-entries/batch/{map_layer_id}', [MapEntriesController::class, 'batch'])->name('map_entries.batch');
Route::post('/map-entries/batch/update', [MapEntriesController::class, 'batchUpdate'])->name('map_entries.batch_update');
Route::get('/map-keys/batch/{map_id}', [MapKeysController::class, 'batch'])->name('map_keys.batch');
Route::post('/map-keys/batch/update', [MapKeysController::class, 'batchUpdate'])->name('map_keys.batch_update');

/* documents */
Route::post('/map-entries/{id}/upload-documents', [MapEntriesController::class, 'uploadDocuments'])->name('map_entries.uploadDocuments');
Route::post('/maps/{id}/upload-documents', [MapsController::class, 'uploadDocuments'])->name('maps.uploadDocuments');

/* resource controllers */
Route::resource('maps', MapsController::Class);
Route::resource('map_layers', MapLayersController::Class);
Route::resource('map_keys', MapKeysController::Class);
Route::resource('map_entries', MapEntriesController::Class);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/import-asv-documents', function () {

    // import/{type}_{question}_{map}_{place}_{notes}.{file_extension}

    $files = Storage::disk('public')->files('import');

    //foreach($files as $key => $item){
    for($i = 0; $i < 200; $i++){

        $item = $files[$i];
        $params = explode('_', $item);

        // get question, map and place from signature
        $question = $params[1];
        $map = $params[2];
        $place = explode('.', $params[3])[0];

        print($item .' '. $question .' '. $map .' '. $place .' '. '<br>');
        
        // move file and create document entry
        $file_name = explode('/', $item)[1];
        $label = implode('.', explode('.', $file_name, -1));

        $document = Document::create([
            'label' => $label,
            'file_name' => $file_name,
            'original_file_name' => $file_name,
            'base_path' => 'documents',
        ]);

        Storage::disk('public')->move($item, 'documents/'.$file_name);

        // get relevant maps with their entries
        $maps = Map::with(['mapEntries']);

        // if question and/or map present, adjust query accordingly
        if($question != '0'){
            $maps->where('label', 'like', '%Frage '.$question.'%');
        } else {
            $maps->where('label', 'like', '%Frage%');
        }
        if($map != '0'){
            $maps->where('label', 'like', '%Karte '.$question.'%');
        } else {
            $maps->where('label', 'like', '%Karte%');
        }

        $maps
            ->whereHas('mapEntries', function($q) use ($place){
                $q->where('map_entries.label', 'like', $place.' %');
            })
            ->get();

        $maps->each(function($el, $i) use ($place, $document){
            print($el->label.'<br>');
            $map_entries = $el->mapEntries()->where('map_entries.label', 'like', $place.' %')->get();

            $map_entries->each(function($el, $i) use ($document){
                print($el->label.'<br>');
                $el->documents()->attach($document);
            });
        });
    }

});


Route::get('/fix-places', function () {
    $map_entries = MapEntry::all();

    $map_entries->each(function($el, $i){
        if($el->place){
            $label = $el->place->label;
            if(Place::where('label', $label)->count() > 1){
                $place = Place::where('label', $label)->orderBy('id', 'asc')->first();
                $el->place_id = $place->id;
                $el->save();
            }
        }
    });
});

Route::get('/remove-leftover-places', function () {
    $map_entries = MapEntry::all();

    $map_entries->each(function($el, $i){
        if($el->place){
            $label = $el->place->label;
            if(Place::where('label', $label)->count() > 1){
                $leftovers = Place::where('label', $label)->doesntHave('mapEntries')->get();
                $leftovers->each(function($leftover, $l){
                    $leftover->delete();
                });
            }
        }
    });
});

Route::get('/fix-place-names', function () {
    $places = Place::all();
    $places->each(function($el, $i){
        $el->label = preg_replace('/([\d]{1,3}a* )/i', '', $el->label);
        $el->save();
    });
});
