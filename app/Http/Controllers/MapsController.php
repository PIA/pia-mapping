<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Map;
use App\Models\MapKey;
use App\Models\MapEntry;
use App\Models\MapLayer;
use App\Models\Place;
use App\Models\Collection;

class MapsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $map = Map::create([
            'label' => $request->label,
            'tiles' => 1,
            'origin' => 'collection'
        ]);
        $map->mapLayers()->create(['label' => 'Ebene 1']);
        $map->collections()->sync([$request->collections]);

        return redirect()->route('maps.images', ['id' => $map->id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $map = Map::create([
            'label' => $request->input('label'),
            'tiles' => 1,
            'origin' => 'mapping'
        ]);
        $map->mapLayers()->create(['label' => 'Ebene 1']);

        return redirect()->route('maps.show', ['map' => $map->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function link(Request $request, $id)
    {
        $map = Map::find($id);
        $map->linkedLayers()->sync(
            $request->input('links')
        );

        return redirect()->route('maps.show', ['map' => $map->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $map = Map::find($id);

        return view('frontend/maps/show', [
            'map' => $map,
            'layers' => $map->mapLayers->merge($map->linkedLayers),
            'maps' => Map::all()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function export($id)
    {
        $map = Map::find($id);

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
    
        $columns = array('id', 'type', 'label', 'description', 'icons', 'keys', 'latitude', 'longitude', 'geojson');
    
        $callback = function() use ($map, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
    
            foreach($map->mapLayers as $layer) {
                foreach($layer->mapEntries as $entry) {

                    $data = [
                        $entry->id,
                        $entry->type,
                        $entry->label,
                        $entry->description,
                    ];

                    $icons = [];
                    $keys = [];

                    foreach($entry->mapKeys as $key) {
                        $keys[] = $key->label;

                        if($key->icon != '') {
                            $icons[] = $key->icon;
                        }
                        if($key->icon_file_name != '') {
                            $icons[] = $key->icon_file_name;
                        }
                    }
                    
                    $data[4] = implode(',', $icons);
                    $data[5] = implode(',', $keys);

                    $data[6] = '';
                    $data[7] = '';
                    $data[8] = '';

                    if ($entry->type == 1) {
                        $data[6] = $entry->place->latitude;
                        $data[7] = $entry->place->longitude;
                    }
                    if ($entry->type == 2) {
                        $data[6] = '';
                        $data[7] = '';
                        $data[8] = $entry->complex_data;
                    }

                    fputcsv($file, $data);
                }
            }
            fclose($file);
        };

        return response()->streamDownload($callback, str_slug($map->label, '_').'.csv');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $map = Map::find($id);

        $map->label = $request->input('label');
        $map->description = $request->input('description');
        $map->tiles = $request->input('tiles');
        
        $map->push();

        return redirect()->route('maps.show', ['map' => $map->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Map::destroy($id);
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function copy(Request $request)
    {
        $old_map = Map::find($request->input('map_id'));
        $map = $old_map->replicate();
        $map->push();

        $map->label = $map->label . ' (Copy ' . date("Y-m-d H:i:s") . ')';

        foreach($old_map->mapKeys as $key) {
           $map->mapKeys()->create($key->toArray());
        }

        foreach($old_map->mapLayers as $old_layer) {
            $layer = $map->mapLayers()->create($old_layer->toArray());

            foreach($old_layer->mapEntries as $old_entry) {
                $entry = $layer->mapEntries()->create($old_entry->toArray());

                /*if($old_entry->place) {
                    $place = $old_entry->place->replicate();
                    $place->push();

                    $entry->place_id = $place->id;
                    $entry->save();
                }*/
            }
        }

        foreach($old_map->linkedLayers as $linked_layer) {
           $map->linked_mapLayers()->create($linked_layer->toArray());
        }

        $map->save();

        return redirect()->route('maps.show', ['map' => $map->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function images($id)
    {
        return view('frontend/maps/images', [
            'map' => Map::find($id)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imagesUpdate(Request $request, $id)
    {
        $map = Map::find($id);
        $layer = $map->mapLayers->first();

        $markers = json_decode($request->markerdata);

        foreach($markers as $key => $marker) {
            $entry = $layer->mapEntries()->create([
                'label' => $marker->alt,
                'type' => 4,
                'image_id' => $marker->id
            ]);

            $place = Place::create([
                'label' => $marker->alt,
                'latitude' => $marker->coordinates->latitude,
                'longitude' => $marker->coordinates->longitude,
                'origin' => 'mapping'
            ]);

            $entry->place_id = $place->id;

            $entry->push();
        }

        return redirect()->route('maps.show', ['map' => $map]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function uploadDocuments(Request $request, $id)
    {
        $map = Map::find($id);

        if($files = $request->file('documents')){
            foreach($files as $file){

                $label = implode('.', explode('.', $file->getClientOriginalName(), -1));
                $original_file_name = $file->getClientOriginalName();
                $file_name = time().'_'.$original_file_name;
                $base_path = 'documents';

                $document = $map->documents()->create([
                    'label' => $label,
                    'file_name' => $file_name,
                    'original_file_name' => $original_file_name,
                    'base_path' => $base_path,
                ]);

                $file->storeAs(
                    'public/'.$base_path, $file_name
                );
            }
        }

        return redirect()->route('maps.show', [$map]);
    }

}
