<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Map;
use App\Models\MapLayer;
use App\Models\MapEntry;
use App\Models\MapKey;
use App\Models\Place;
use App\Models\Image;

class MapEntriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $layer = MapLayer::find($request->input('layer_id'));
        $entry = $layer->mapEntries()->create([
            'label' => $request->input('label'),
            'type' => $request->input('type')
        ]);
        
        /*if($entry->type == 1) {
            $place = Place::create([
                'label' => $entry->label,
                'origin' => 'mapping'
            ]);

            $entry->place_id = $place->id;
            $entry->save();
        }*/
        if($entry->type == 3) {
            $image = Image::create([
                'title' => $entry->label,
                'base_path' => 'upload'
            ]);

            $entry->image_id = $image->id;
            $entry->save();
        }
        if($entry->type == 4) {
            $image = Image::create([
                'title' => $entry->label,
                'base_path' => 'upload'
            ]);
            $place = Place::create([
                'label' => $entry->label,
                'origin' => 'mapping'
            ]);

            $entry->place_id = $place->id;
            $entry->image_id = $image->id;
            $entry->save();
        }

        return redirect()->route('map_entries.show', ['map_entry' => $entry->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entry = MapEntry::find($id);

        return view('frontend/map-entries/show', [
            'editable_entry' => $entry,
            'keys' => $entry->mapLayer->map->mapKeys,
            'places' => Place::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entry = MapEntry::find($id);

        $entry->label = $request->input('label');
        $entry->description = $request->input('description');
        $entry->map_layer_id = $request->input('layer_id');

        $entry->mapKeys()->sync($request->input('keys'));
        
        if($entry->type == 1) {

            if($entry->place_id == $request->place_id && $request->place_id != ''){
                $entry->place->latitude = $request->input('latitude');
                $entry->place->longitude = $request->input('longitude');
            } else {
                $entry->place_id = $request->place_id;
            }

            $entry->push();

            if(!$entry->place) {
                if($request->latitude != '' && $request->longitude != '' && $request->place_id == ''){
                    $place = Place::create([
                        'label' => $entry->label,
                        'origin' => 'mapping',
                        'latitude' => $request->latitude,
                        'longitude' => $request->longitude
                    ]);
                    $entry->place_id = $place->id;
                    $entry->save();
                }
            }
        }

        if(in_array($entry->type, [3, 4])) {
            $image = Image::find($entry->image_id);
            $image->title = $entry->label;

            if($request->file('image')){
                // $curl = curl_init();

                // curl_setopt_array($curl, array(
                // CURLOPT_URL => 'https://sipi.participatory-archives.ch/server/upload-pia.elua',
                // CURLOPT_RETURNTRANSFER => true,
                // CURLOPT_ENCODING => '',
                // CURLOPT_MAXREDIRS => 10,
                // CURLOPT_TIMEOUT => 0,
                // CURLOPT_FOLLOWLOCATION => true,
                // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                // CURLOPT_CUSTOMREQUEST => 'POST',
                // // @TODO: fix those damn ssl problems
                // CURLOPT_SSL_VERIFYHOST => false,
                // CURLOPT_SSL_VERIFYPEER => false,
                // CURLOPT_POSTFIELDS => array(
                //         'Datei'=> new \CURLFile(
                //             $request->file('image')->getPathName()
                //         ),
                //         'Cuse_sop' => 'yes'
                //     ),
                // ));

                // $response = curl_exec($curl);

                // $data = json_decode($response);

                // $image->signature = $data->signature;

                $original_file_name = $request->file('image')->getClientOriginalName();
                $file_name = time().'__'.$original_file_name;

                $image->original_file_name = $original_file_name;
                $image->file_name = $file_name;

                $request->file('image')->storeAs(
                    'public/uploads', $file_name
                );
            }

            $image->save();
        }

        if(in_array($entry->type, [2, 3])) {
            $entry->complex_data = $request->input('complex_data');
        }

        if($entry->type == 4) {
            $entry->place->label = $entry->label;
            $entry->place->latitude = $request->input('latitude');
            $entry->place->longitude = $request->input('longitude');
        }

        $entry->push();

        return redirect()->route('map_entries.show', ['map_entry' => $entry->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $rows   = array_map('str_getcsv', $this->remove_utf8_bom(file($request->file('file')->getPathName())));
        $header = array_shift($rows);
        $csv    = array();

        foreach($rows as $row) {
            $csv[] = array_combine($header, $row);
        }

        $map = Map::find($request->input('map_id'));
        $layer = $map->mapLayers()->create([
            'label' => $request->file('file')->getClientOriginalName()
        ]);

        foreach($csv as $key => $row) {

            $type = 1;
            if(isset($row['type'])){
                $type = $row['type'];
            }
    
            $label = '';
            if(isset($row['label'])){
                $label = $row['label'];
            }
    
            $description = '';
            if(isset($row['description'])){
                $description = $row['description'];
            }

            $entry = $layer->mapEntries()->create([
                'label' => $label,
                'description' => $description,
                'type' => $type
            ]);
            
            if($type == 1) {
                $place = Place::create([
                    'label' => $label,
                    'latitude' => $row['latitude'],
                    'longitude' => $row['longitude'],
                    'origin' => 'mapping'
                ]);

                $entry->place_id = $place->id;
                $entry->save();
            }
            
            if($type == 2) {
                $entry->complex_data = $row['geojson'];
                $entry->save();
            }

            $keys = [];

            foreach(explode(',', $row['keys']) as $key => $label) {
                $label = trim($label);
                if(!MapKey::where('label', $label)
                    ->where('map_id', $map->id)->exists()) {
                    $keys[] = $map->mapKeys()->create([
                        'label' => $label
                    ])->id;
                } else {
                    $keys[] = MapKey::where('label', $label)
                            ->where('map_id', $map->id)->first()->id;
                }
            }

            $entry->mapKeys()->sync($keys);
        }

        return redirect()->route('maps.show', ['map' => $map->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function batch($id)
    {
        $layer = MapLayer::find($id);

        return view('frontend/map-entries/batch', [
            'layer' => $layer,
            'entries' => $layer->mapEntries,
            'keys' => $layer->map->mapKeys
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function batchUpdate(Request $request)
    {
        foreach($request->all() as $key => $input) {
            if(strpos($key, 'map-entry') !== false){
                $entry = MapEntry::find($input);
                $entry->label = $request->input('label-'.$input);
                $entry->mapKeys()->sync($request->input('keys-'.$input));

                $entry->save();
            }
        }
        return redirect()->route('map_entries.batch', ['map_layer_id' => $request->input('map_layer_id')]);
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function uploadDocuments(Request $request, $id)
    {
        $entry = MapEntry::find($id);

        if($files = $request->file('documents')){
            foreach($files as $file){

                $label = implode('.', explode('.', $file->getClientOriginalName(), -1));
                $original_file_name = $file->getClientOriginalName();
                $file_name = time().'_'.$original_file_name;
                $base_path = 'documents';

                $document = $entry->documents()->create([
                    'label' => $label,
                    'file_name' => $file_name,
                    'original_file_name' => $original_file_name,
                    'base_path' => $base_path,
                ]);

                $file->storeAs(
                    'public/'.$base_path, $file_name
                );
            }
        }

        return redirect()->route('map_entries.show', [$entry]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        MapEntry::destroy($id);
        return redirect()->route('maps.show', ['map' => $request->input('map_id')]);
    }

    protected function remove_utf8_bom($text)
    {
        $bom = pack('H*','EFBBBF');
        $text = preg_replace("/^$bom/", '', $text);
        return $text;
    }
}
