<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Map;
use App\Models\MapKey;

class MapKeysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $map = Map::find($request->input('map_id'));
        $key = $map->mapKeys()->create([
            'label' => $request->input('label')
        ]);

        return redirect()->route('map_keys.show', ['map_key' => $key->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('frontend/map-keys/show', [
            'key' => MapKey::find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $key = MapKey::find($id);

        $key->label = $request->input('label');
        $key->icon = $request->input('icon');

        if($request->file('icon-file')){

            $original_icon_file_name = $request->file('icon-file')->getClientOriginalName();
            $icon_file_name = time().'__'.$original_icon_file_name;

            $key->original_icon_file_name = $original_icon_file_name;
            $key->icon_file_name = $icon_file_name;

            $request->file('icon-file')->storeAs(
                'public/legend-icons', $icon_file_name
            );
        }

        $key->save();

        return redirect()->route('map_keys.show', ['map_key' => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $rows   = array_map('str_getcsv', $this->remove_utf8_bom(file($request->file('file')->getPathName())));
        $header = array_shift($rows);
        $csv    = array();

        foreach($rows as $row) {
            $csv[] = array_combine($header, $row);
        }

        $map = Map::find($request->input('map_id'));

        foreach($csv as $key => $row) {
            if($row['label'] != '' && !MapKey::where([
                ['label', '=', $row['label']],
                ['map_id', '=', $map->id]
            ])->exists()) {
                $map->mapKeys()->create([
                    'label' => $row['label']
                ]);
            }
        }

        return redirect()->route('maps.show', ['map' => $map->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function batch($id)
    {
        $map = Map::find($id);

        return view('frontend/map-keys/batch', [
            'map' => $map,
            'keys' => $map->mapKeys
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function batchUpdate(Request $request)
    {
        foreach($request->all() as $key => $input) {
            if(strpos($key, 'map-key-id') !== false){
                $key = MapKey::find($input);

                if($request->input('map-key-label-'.$input)){
                    $key->label = $request->input('map-key-label-'.$input);
                }

                if($request->file('icon-file-'.$input)){

                    $key->icon = '';

                    $original_icon_file_name = $request->file('icon-file-'.$input)->getClientOriginalName();
                    $icon_file_name = time().'__'.$original_icon_file_name;

                    $key->original_icon_file_name = $original_icon_file_name;
                    $key->icon_file_name = $icon_file_name;

                    $request->file('icon-file-'.$input)->storeAs(
                        'public/legend-icons', $icon_file_name
                    );
                }

                $key->save();
            }
        }

        return redirect()->route('maps.show', ['map' => $request->input('map_id')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        MapKey::destroy($id);
        return redirect()->route('maps.show', ['map' => $request->input('map_id')]);
    }

    protected function remove_utf8_bom($text)
    {
        $bom = pack('H*','EFBBBF');
        $text = preg_replace("/^$bom/", '', $text);
        return $text;
    }
}
