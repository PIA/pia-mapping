<header class="flex fixed justify-between w-full p-4 py-2 bg-white border-b border-black" style="z-index: 500;">
    <h1 class="text-xl">PIA Mapping Prototype</h1>

    <div class="flex">
        <a href="/" class="hover:underline">Dashboard</a>
        {{ $slot }}
    </div>

</header>