<button
{{ $attributes->merge(['class' => 'border border-gray-900 bg-white hover:bg-gray-900 hover:text-white p-1 px-6 transition uppercase text-xs']) }}>
    {{ $slot }}
</button>