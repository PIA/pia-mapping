@extends('frontend/base')

@section('content')

<x-header>
    <div class="flex flex-1 justify-end">
        <a href="{{ route('maps.show', ['map' => $map_layer->map->id]) }}" class="ml-2 hover:underline">Map</a>
    </div>
</x-header>

<div class="flex min-h-screen">
    <aside class="w-1/3 shadow-2xl z-40" style="padding-top: 45px;">
        <div>
            <form method="POST" action="{{ route('map_layers.update', ['map_layer' => $map_layer->id]) }}">
                @csrf
                @method('patch')

                <input class="py-2 px-4 text-xl border-b border-black bg-gray-100 flex justify-between font-bold w-full" type="text" name="label" placeholder="Label" value="{{ $map_layer->label }}" required>

                <div class="p-4">
                    <span class="mb-4 inline-block">Show layer <em>from/to</em> zoom level.</span>
                    <div class="flex">
                        <div class="w-1/2 pr-2">
                            <label for="zoom_min" class="text-xs block">From</label>
                            <select name="zoom_min" id="zoom_min" class="w-full">
                                @for ($i = 0; $i <= 18; $i++)
                                    <option value="{{ $i }}" {{ $map_layer->zoom_min == $i ? 'selected' : '' }}>
                                        {{ $i }}
                                        {{ $i == 0 ? ' - Globe' : '' }}
                                        {{ $i == 8 ? ' - Country' : '' }}
                                        {{ $i == 11 ? ' - State' : '' }}
                                        {{ $i == 14 ? ' - City' : '' }}
                                        {{ $i == 18 ? ' - Closest' : '' }}
                                    </option>
                                @endfor
                            </select>
                        </div>
                        <div class="w-1/2">
                            <label for="zoom_max" class="text-xs block">To</label>
                            <select name="zoom_max" id="zoom_max" class="w-full">
                                @for ($i = 0; $i <= 18; $i++)
                                    <option value="{{ $i }}" {{ ($map_layer->zoom_max ?? 18) == $i ? 'selected' : '' }}>
                                        {{ $i }}
                                        {{ $i == 0 ? ' - Globe' : '' }}
                                        {{ $i == 8 ? ' - Country' : '' }}
                                        {{ $i == 11 ? ' - State' : '' }}
                                        {{ $i == 14 ? ' - City' : '' }}
                                        {{ $i == 18 ? ' - Closest' : '' }}
                                    </option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>

                <div class="text-right mt-4 px-4 flex justify-between">
                    <a href="{{ route('maps.show', ['map' => $map_layer->map->id]) }}" class="hover:underline">Back</a>
                    <x-fe-button>Save</x-fe-button>
                </div>
            </form>
        </div>
        <form method="POST" action="{{ route('map_layers.destroy', ['map_layer' => $map_layer->id]) }}" onsubmit="return confirm('Do you really want to delete this layer and all its entries?');">
            @csrf
            @method('delete')
            <input type="hidden" name="map_id" value="{{ $map_layer->map->id }}">
            <button class="absolute bottom-4 border border-red-500 bg-red-500 text-white hover:bg-white hover:text-red-500 p-1 px-6 transition uppercase text-xs ml-4" type="submit">Delete Layer</button>
        </form>
    </aside>

    <div class="w-2/3 h-screen bg-gray-200"></div>
</div>
@endsection