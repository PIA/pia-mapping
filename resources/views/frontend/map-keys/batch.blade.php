@extends('frontend/base')

@section('content')

<x-header>
    <div class="flex flex-1 justify-end">
        <a href="{{ route('maps.show', ['map' => $map->id]) }}" class="ml-2 hover:underline">Map</a>
    </div>
</x-header>

<div class="flex min-h-screen">
    <aside class="w-1/3 shadow-2xl z-40" style="padding-top: 45px;">
        <div>
            <form id="map_keys_form" enctype="multipart/form-data" method="POST" action="{{ route('map_keys.batch_update') }}">
                @csrf

                <input type="hidden" name="map_id" value="{{ $map->id }}">

                @foreach ($keys as $key)
                    <div class="py-2 px-4 border-b">
                        <input type="hidden" name="map-key-id-{{ $key->id }}" value="{{ $key->id }}">
                        <input type="text" class="block w-full" name="map-key-label-{{ $key->id }}" value="{{ $key->label }}">
                        @if($key->icon_file_name)<img src="/storage/legend-icons/{{ $key->icon_file_name }}" class="inline-block" style="width: 14px; position: relative; top: -3px;">@else &mdash; @endif
                        <input class="text-xs" type="file" name="icon-file-{{ $key->id }}" accept="image/*">
                    </div>
                @endforeach
                
                <div class="text-right mt-4 px-4 flex justify-between">
                    <a href="{{ route('maps.show', ['map' => $map->id]) }}" class="hover:underline">Back</a>
                    <x-fe-button>Save</x-fe-button>
                </div>
            </form>
        </div>
    </aside>

    <div class="flex-1 h-full w-2/3 fixed right-0" style="padding-top: 45px;">
        @include('frontend/maps/render', [
            'map' => $map,
            'layers' => $map->mapLayers
        ])
    </div>
</div>
@endsection