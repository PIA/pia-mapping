@extends('frontend/base')

@section('content')

<x-header>
    <div class="flex flex-1 justify-end">
        <a href="{{ route('maps.show', ['map' => $key->map->id]) }}" class="ml-2 hover:underline">Map</a>
    </div>
</x-header>

<div class="flex min-h-screen">
    <aside class="w-1/3 shadow-2xl z-40" style="padding-top: 45px;">
        <div>
            <form method="POST" enctype="multipart/form-data" action="{{ route('map_keys.update', ['map_key' => $key->id]) }}">
                @csrf
                @method('patch')

                <input class="py-2 px-4 text-xl border-b border-black bg-gray-100 flex justify-between font-bold w-full" type="text" name="label" placeholder="Label" value="{{ $key->label }}" required>

                <div x-data="{show_picker: false, emoji: ''}" class="py-2 px-4 relative w-full justify-between border-b border-gray-400">
                    <emoji-picker class="absolute top-0 right-0" x-show="show_picker" @emoji-click="() => {
                        show_picker = false;
                        $refs.emojiicon.value = event.detail.unicode;    
                    }"></emoji-picker>
                    <input x-ref="emojiicon" class="w-full mb-2" type="text" name="icon" placeholder="Icon" value="{{ $key->icon }}">
                    <x-fe-button type="button" @click="show_picker = ! show_picker; console.log(show_picker)">Emoji</x-fe-button>
                </div>

                <div x-data class="py-2 px-4 w-full justify-between border-b border-gray-400">
                    <span class="file-name {{ $key->original_icon_file_name == '' ? 'is-hidden' : '' }} pia-icon_file-label mb-2 w-full inline-block">
                        {{$key->original_icon_file_name}}
                    </span>
                    <x-fe-button type="button" @click="$refs.file.click()">Choose image file</x-fe-button>
                    <input x-ref="file" class="file-input pia-icon_file-input hidden" type="file" name="icon-file" accept="image/*">
                </div>


                <div class="text-right mt-4 px-4 flex justify-between">
                    <a href="{{ route('maps.show', ['map' => $key->map->id]) }}" class="hover:underline">Back</a>
                    <x-fe-button>Save</x-fe-button>
                </div>
            </form>
        </div>
        <form method="POST" action="{{ route('map_keys.destroy', ['map_key' => $key->id]) }}" onsubmit="return confirm('Do you really want to delete this map key?');">
            @csrf
            @method('delete')
            <input type="hidden" name="map_id" value="{{ $key->map->id }}">
            <button class="absolute bottom-4 border border-red-500 bg-red-500 text-white hover:bg-white hover:text-red-500 p-1 px-6 transition uppercase text-xs ml-4" type="submit">Delete Map Key</button>
        </form>
    </aside>

    <div class="w-2/3 h-screen bg-gray-200"></div>
</div>
@endsection

@section('scripts')
<script type="module" src="https://cdn.jsdelivr.net/npm/emoji-picker-element@^1/index.js"></script>
<script>

    let icon_file_input = document.querySelector('.pia-icon_file-input'),
        icon_file_label = document.querySelector('.pia-icon_file-label');

    icon_file_input.addEventListener('change', e => {
        icon_file_label.classList.remove('is-hidden');
        icon_file_label.innerHTML = icon_file_input.files[0].name;
    });

</script>
@endsection