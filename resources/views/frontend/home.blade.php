@extends('frontend/base')

@section('content')

    <x-header>
        <div class="ml-4 flex justify-end">
            <form method="POST" action="{{ route('maps.store') }}">
                @csrf
                <input class="input" type="hidden" name="label" placeholder="Label" value="Neue Karte">
                <x-fe-button>New Map</x-fe-button>
            </form>
            <form method="POST" action="{{ route('maps.copy') }}" class="ml-4">
                @csrf
                <x-fe-button>Copy</x-fe-button>
                <span>></span>
                <select name="map_id" class="text-xs border-b inline-block p-1 px-6 border-gray-900">
                    @foreach ($maps_mapping as $map)
                        <option value="{{ $map->id }}">{{ $map->label }}</option>
                    @endforeach
                </select>
            </form>
        </div>
    </x-header>

    <div class="p-4 py-20 flex">
        <div class="w-1/2 pr-4">
            <h2 class="text-2xl mb-4">Via Mapping App</h2>
            @foreach ($maps_mapping as $map)
                <div class="mb-4">
                    <a href="{{ route('maps.show', ['map' => $map->id]) }}" class="border-b border-black hover:bg-white hover:text-black py-2 flex justify-between bg-black text-white transition px-2">
                        <h3>{{ $map->label }}</h3>
                        <span>→</span>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="w-1/2 pl-4">
            <h2 class="text-2xl mb-4">Via Collections</h2>
            @foreach ($maps_collection as $map)
                <div class="mb-4">
                    <a href="{{ route('maps.show', ['map' => $map->id]) }}" class="border-b border-black hover:bg-white hover:text-black py-2 flex justify-between bg-black text-white transition px-2">
                        <h3>{{ $map->label }}</h3>
                        <span>→</span>
                    </a>
                </div>
            @endforeach
        </div>
    </div>

    <style>
        .map {
            min-height: 260px;
        }
        .leaflet-control-container {
            display: none;
        }
    </style>
@endsection