<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PIA Mapping</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link href="/css/pia.css" rel="stylesheet">
    <link href="/fonts/apercu/stylesheet.css" rel="stylesheet">

    <link rel="stylesheet" href="/node_modules/leaflet/dist/leaflet.css">
    <link rel="stylesheet" href="/node_modules/@geoman-io/leaflet-geoman-free/dist/leaflet-geoman.css" /> 
    <link rel="stylesheet" href="/node_modules/leaflet-distortableimage/dist/vendor.css">
    <link rel="stylesheet" href="/node_modules/leaflet-distortableimage/dist/leaflet.distortableimage.css">
    <link rel="stylesheet" href="/node_modules/leaflet.markercluster/dist/MarkerCluster.css">
    <link rel="stylesheet" href="/node_modules/leaflet.markercluster/dist/MarkerCluster.Default.css">
    <link rel="stylesheet" href="/node_modules/slim-select/dist/slimselect.min.css">

    @yield('styles')

</head>
<body>
    
    @yield('content')

    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <script src="/node_modules/leaflet/dist/leaflet.js"></script>
    <script src="/node_modules/leaflet-providers/leaflet-providers.js"></script>
    <script src="/node_modules/@geoman-io/leaflet-geoman-free/dist/leaflet-geoman.min.js"></script>
    <script src="/node_modules/leaflet-distortableimage/dist/vendor.js"></script>
    <script src="/node_modules/leaflet-distortableimage/dist/leaflet.distortableimage.js"></script>
    <script src="/node_modules/leaflet.markercluster/dist/leaflet.markercluster.js"></script>
    <script src="/node_modules/slim-select/dist/slimselect.min.js"></script>

    <script>
        window.maps_holder = [];
    </script>

    @yield('scripts')

</body>
</html>