@php $map_slug = str_slug('m_'.$map->label.'_'.$map->id, '_') @endphp

<div id="{{ $map_slug }}" class="map h-full"></div>

<style>

emoji-picker {
    position: absolute;
    z-index: 10;
    display: none;
}
emoji-picker.show {
    display: block;
}
.legend.leaflet-control {
    display: inline-block;
    background-color: rgba(255, 255, 255, 1);
    padding: 10px;

    max-width: 50px;
    max-height: 50px;
    width: 50px;
    overflow: hidden;
}
.legend.leaflet-control .icon {
    font-size: 40px;
    line-height: 40px;
    position: absolute;
    top: 5px;
    left: 5px;
}
.legend.leaflet-control label {
    display: none;
}
.legend.leaflet-control:hover {
    max-width: 2000px;
    max-height: 2000px;
    width: auto;
    background-color: rgba(255, 255, 255, 1);
}
.legend.leaflet-control:hover .icon {
    display: none;
}
.legend.leaflet-control:hover label {
    display: inline-block;
}

</style>

<script>

document.addEventListener('DOMContentLoaded', function () {

    L.Map.addInitHook(function () {
        maps_holder.push(this);
    });

    let center = [46.818188, 8.227512],
        map = L.map('{{ $map_slug }}', {
            center: center,
            zoom: 8,
            doubleClickZoom: false
        }),
        layers = {}, zoom_management = {}, keys = {};

    map.doubleClickZoom.disable(); 

    @include('frontend/maps/tiles', ['map' => $map])

    @foreach($layers as $layer)

        @php $layer_slug = str_slug('l_'.$layer->label.'_'.$layer->id, '_') @endphp

        let {{ $layer_slug }} = L.markerClusterGroup({
                'maxClusterRadius': 1
            });

        layers['{{ $layer->label }}'] = {{ $layer_slug }};
        zoom_management['{{ $layer->label }}'] = {
            'target': {{ $layer_slug }},
            'zoom_min': {{ $layer->zoom_min ?? 0 }},
            'zoom_max': {{ $layer->zoom_max ?? 18 }}
        };

        @foreach($layer->mapEntries as $entry)
            @if(isset($editable_entry))
                @if($editable_entry->id == $entry->id)
                    @if (in_array($editable_entry->type, [1, 4]))
                        @include('frontend/maps/entry-editable-1', ['entry' => $entry])
                    @endif
                    @if ($editable_entry->type == 2)
                        @include('frontend/maps/entry-editable-2', ['entry' => $entry])
                    @endif
                    @if ($editable_entry->type == 3)
                        @include('frontend/maps/entry-editable-3', ['entry' => $entry])
                    @endif
                    @if ($editable_entry->type == 4)
                        @include('frontend/maps/entry-editable-4', ['entry' => $entry])
                    @endif
                @else
                    @include('frontend/maps/entry', ['entry' => $entry, 'opacity' => '.5'])
                @endif
            @else
                @include('frontend/maps/entry', ['entry' => $entry])
            @endif
        @endforeach

        {{ $layer_slug }}.addTo(map);

    @endforeach

    L.PM.setOptIn(true);

    L.control.layers(null, layers).addTo(map);

    // TODO: Comment
    if(Object.keys(keys).length > 0) {
        var legend = L.control({position: 'bottomleft'});
        legend.onAdd = function (map) {

            let legend_content = L.DomUtil.create('div', 'legend');

            legend_content.innerHTML = '<span class="icon">🗺️</span>';

            for (let key in keys) {
                legend_content.innerHTML += '<label><input class="key" type="checkbox" value="'+key+'" checked/> '+keys[key].label+'</label><br>';
            }

            legend_content.querySelectorAll('input.key').forEach(el => {
                el.addEventListener('change', evt => {
                    let entries = keys[evt.target.value].entries;

                    for(let e in entries) {
                        let entry = entries[e];

                        if(evt.target.checked){
                            layers[entry.layer].addLayer(entry);
                        } else {
                            layers[entry.layer].removeLayer(entry);
                        }
                    }
                    
                    for(let l in layers){
                        layers[l].refreshClusters();
                    }
                })
            })

            return legend_content;
        };
        legend.addTo(map);
    }

    zoomed();

    map.on('zoom', function zoomendEvent(evt) {
        zoomed();
    });

    function zoomed(){
        let current_zoom = map.getZoom();
        for(let l in zoom_management){
            let layer = zoom_management[l];
            if(current_zoom < layer.zoom_min || current_zoom > layer.zoom_max){
                map.removeLayer(layer.target);
            } else {
                map.addLayer(layer.target)
            }
        }
    }

});

</script>