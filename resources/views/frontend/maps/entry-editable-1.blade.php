let entry_{{ $editable_entry->id }} = undefined;

map.pm.addControls({
    position: 'topleft',
    drawCircleMarker: false,
    drawPolyline: false,
    drawRectangle: false,
    drawPolygon: false,
    drawCircle: false,
    editMode: false,
    cutPolygon: false,
    rotateMode: false,
});

map
    .on('pm:create', e => {
        entry_{{ $editable_entry->id }} = e.layer;
        if(entry_{{ $editable_entry->id }} instanceof L.Marker) {
            map.pm.disableDraw('Marker');
        }

        fill_latlng(entry_{{ $editable_entry->id }}._latlng);
        toggle_edit();
    })
    .on('pm:remove', e => {
        entry_{{ $editable_entry->id }} = undefined;
        fill_latlng({
            lat: '',
            lng: ''
        })
        toggle_edit();
    })
    .on('pm:globaldragmodetoggled', e => {
        fill_latlng(entry_{{ $editable_entry->id }}._latlng);
    });

@if (in_array($editable_entry->type, [1, 4]) && $editable_entry->place && $editable_entry->place->latitude)
    {{ $lat = $editable_entry->place->latitude }}
    {{ $lng = $editable_entry->place->longitude }}

    entry_{{ $editable_entry->id }} = new L.marker([{{$lat}}, {{$lng}}], { pmIgnore: false });

    let icons_{{ $editable_entry->id }} = [];

    @foreach ($editable_entry->mapKeys as $key)
        @if ($key->icon)
            icons_{{ $editable_entry->id }}.push('<span>{{$key->icon}}</span>');
        @elseif ($key->icon_file_name != '')
            icons_{{ $editable_entry->id }}.push('<img src="/storage/legend-icons/{{$key->icon_file_name}}"/>');
        @endif
    @endforeach

    if(icons_{{ $editable_entry->id }}.length) {
        entry_{{ $editable_entry->id }}
            .setIcon(
                L.divIcon({
                    html: icons_{{ $editable_entry->id }}.join(''),
                    className: 'legend-entry-icon'
                })
            );
    }

    entry_{{ $editable_entry->id }}
        .addTo(map).bindTooltip('{{ $editable_entry->label }}');
    
    toggle_edit();
@endif

function toggle_edit(){
    if(L.PM.Utils.findLayers(map).length) {
        map.pm.Toolbar.setButtonDisabled('drawMarker', true);
    } else {
        map.pm.Toolbar.setButtonDisabled('drawMarker', false);
    }
}

function fill_latlng(_latlng){
    document.querySelector('input[name="latitude"]').value = _latlng.lat;
    document.querySelector('input[name="longitude"]').value = _latlng.lng;
}