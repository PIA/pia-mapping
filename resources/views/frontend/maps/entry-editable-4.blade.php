let image_input = document.querySelector('.pia-image-input'),
    image_label = document.querySelector('.pia-image-label');

image_input.addEventListener('change', e => {

    image_label.classList.remove('is-hidden');
    image_label.innerHTML = image_input.files[0].name;

});