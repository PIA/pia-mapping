@extends('frontend/base')

@section('content')
<div class="flex max-h-screen min-h-screen" x-data="app">
    <div class="w-1/2 p-6 max-h-screen overflow-scroll">
        <div id="images">
            <div class="grid gap-4 grid-flow-row grid-cols-2">
                <template x-for="image in images" :key="image.id">
                    <div x-data="{show_meta: false}">
                        <div class="relative cursor-move"
                            @mouseover="show_meta = true" @mouseout="show_meta = false"
                            >
                            <img class="w-full"
                                :src="`${image.links.related}full/480,/0/default.jpg`"
                                :data-id="image.id"
                                :alt="image.attributes.title"
                                @dragstart="dragstart">
                            <div class="meta absolute bottom-0 left-0 p-2 w-full text-xs underline bg-gradient-to-r from-yellow-400 via-red-500 to-pink-500"
                                x-show="show_meta">
                                <div class="links">
                                    <a target="_blank" :href="image.links.related" class="mr-2">IIIF</a>
                                    <a target="_blank" :href="image.links.self" class="mr-2">JSON</a>
                                    <a target="_blank" :href="`https://data.dasch.swiss/resources/${image.attributes.salsah_id}`">SALSAH</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </template>
            </div>
        </div>
    </div>
    <div class="w-1/2 max-h-screen overflow-scroll border-l-4 border-black">
        <div id="map" class="map h-full w-full"
            @drop="drop" @dragover="dragover"></div>
        <button type="button" style="z-index: 1000;"
            class="absolute top-4 right-4 rounded-full bg-black text-white py-1 px-3"
            @click="save">Save</button>
    </div>

    <form action="{{ route('maps.imagesUpdate', [$map]) }}" method="post" x-ref="markerform">
        @csrf
        @method('patch')
        <input type="hidden" name="markerdata" x-ref="markerdata">
    </form>
</div>
@endsection

@section('scripts')
<script>

    let custom_entry = L.Marker.extend({
        imageid: 0,
        alt: ''
    });

    document.addEventListener('alpine:init', () => {

        Alpine.data('app', () => ({

            map,
            
            api_url: '{{ env('API_URL') }}',
            loading: true,
            show_rendering: false,

            images: [],
            collection: {
                id: '{{ $map->collections()->first()->id }}',
                label: ''
            },

            // methods
            init() {
                this.fetch_collection()

                let map = L.map('map', {
                    center: [46.818188, 8.227512],
                    zoom: 8,
                    doubleClickZoom: false
                })

                @include('frontend/maps/tiles', ['map' => $map]);

                map.pm.addControls({
                    position: 'topleft',
                    drawMarker: false,
                    drawCircleMarker: false,
                    drawPolyline: false,
                    drawRectangle: false,
                    drawPolygon: false,
                    drawCircle: false,
                    editMode: false,
                    cutPolygon: false,
                    rotateMode: false,
                });

                this.map = map;

                /*let saving = setInterval(() => {
                    var url = "{{ route('maps.imagesUpdate', [$map]) }}";

                    var xhr = new XMLHttpRequest();
                    xhr.open("PATCH", url);

                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader("X-CSRF-TOKEN", document.querySelector('meta[name="csrf-token"]').content);

                    xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        // nice
                    }};

                    xhr.send(JSON.stringify({
                        id: '{{ $map->id }}',
                        content: '',
                    }));

                }, 5000);*/
            },

            fetch_collection() {
                this.loading = true;
                fetch(`${this.api_url}collections/${this.collection.id}?include=images`)
                    .then(response => response.json())
                    .then(response => {
                        this.collection.label = response.data.attributes.label;
                        this.images = response.included;
                        this.loading = false;
                    });
            },

            dragstart(evt) {
                let img = evt.target,
                    data = {
                        'id': img.dataset.id,
                        'src': img.src,
                        'alt': img.alt
                    };

                evt.dataTransfer.setData('text', JSON.stringify(data));
            },

            dragover(evt) {
                evt.preventDefault();
                evt.dataTransfer.dropEffect = 'move';
            },

            drop(evt) {
                evt.preventDefault();

                let img = JSON.parse(evt.dataTransfer.getData("text/plain"));

                var rect = evt.target.getBoundingClientRect();
                var x = evt.clientX - rect.left;
                var y = evt.clientY - rect.top;


                let coordinates = this.map.containerPointToLatLng(L.point([x, y]));

                let entry = new custom_entry(coordinates).addTo(this.map);

                entry.imageid = img.id;
                entry.alt = img.alt;

                entry.bindPopup(
                    `<img src="${img.src}" alt="${img.alt}"/>`,
                        {
                            minWidth: 320,
                            closeButton: false
                        }
                    )
                    .on('mouseover', function (e) {
                        this.openPopup();
                    });
            },

            save() {
                let data = [];

                this.map.eachLayer(function (layer) { 
                    if(layer instanceof L.Marker) {
                        data.push({
                            id: layer.imageid,
                            alt: layer.alt,
                            coordinates: {
                                latitude: layer.getLatLng().lat,
                                longitude: layer.getLatLng().lng
                            }
                        })
                    }
                });

                this.$refs.markerdata.value = JSON.stringify(data);
                this.$refs.markerform.submit();
            }
        }));
    });

</script>
@endsection