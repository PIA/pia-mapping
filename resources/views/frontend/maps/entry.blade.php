@if (in_array($entry->type, [1, 4]) && $entry->place && $entry->place->latitude)
    {{ $lat = $entry->place->latitude }}
    {{ $lng = $entry->place->longitude }}

    custom_entry = L.Marker.extend({
        options: { 
            layer: '',
            opacity: {{ $opacity ?? '1' }}
        }
    });

    let entry_{{ $entry->id }} = new custom_entry([{{$lat}}, {{$lng}}]),
        icons_{{ $entry->id }} = [];

    @foreach ($entry->mapKeys as $key)
        if(!keys['{{$key->id}}']) {
            keys['{{$key->id}}'] = {
                'label': '{{$key->label}}',
                'entries': []
            }
        }

        @if ($key->icon)
            icons_{{ $entry->id }}.push('<span>{{$key->icon}}</span>');
        @elseif ($key->icon_file_name != '')
            icons_{{ $entry->id }}.push('<img src="/storage/legend-icons/{{$key->icon_file_name}}"/>');
        @endif

        keys['{{$key->id}}']['entries'].push(entry_{{ $entry->id }})
    @endforeach

    if(icons_{{ $entry->id }}.length) {
        entry_{{ $entry->id }}
            .setIcon(
                L.divIcon({
                    html: icons_{{ $entry->id }}.join(''),
                    className: 'legend-entry-icon'
                })
            );
    }

    entry_{{ $entry->id }}
        .addTo({{ $layer_slug }}).bindTooltip('{{ $entry->label }}')
        .addEventListener('click', function(e) {
            window.location = '{{ route('map_entries.show', ['map_entry' => $entry->id]) }}';
        });

    entry_{{ $entry->id }}.layer = '{{ $layer->label }}';

@endif
@if ($entry->type == 2 && $entry->complex_data)
    data = {!! $entry->complex_data !!};
    data.features.forEach((el, i) => {
        let geo_layer = L.geoJSON(el, {
            pointToLayer: (feature, latlng) => {
                if (feature.properties.radius) {
                    let circle = new L.Circle(latlng, feature.properties.radius, {opacity: {{ $opacity ?? '1' }}});

                    return circle;
                } else {
                    return new L.Marker(latlng, {opacity: {{ $opacity ?? '1' }}});
                }
            },
            onEachFeature: (feature, layer) => {
                layer.addTo({{ $layer_slug }});
            },
            opacity: {{ $opacity ?? '1' }}
        });
        geo_layer.addEventListener('click', function(e) {
            window.location = '{{ route('map_entries.show', ['map_entry' => $entry->id]) }}';
        })
    })
@endif
@if ($entry->type == 3 && $entry->image)
    L.distortableImageOverlay(`/storage/uploads/{{$entry->image->file_name}}`, {
        editable: false,
        corners: {!! $entry->complex_data !!},
    }).addTo({{ $layer_slug }}).bindTooltip('{{ $entry->label }}');
@endif
@if ($entry->type == 4 && $entry->image)
    entry_{{ $entry->id }}
        .bindPopup(
            `<img src="/storage/uploads/{{$entry->image->file_name}}"/>`,
            {
                minWidth: 320,
                closeButton: false
            }
        )
        .on('mouseover', function (e) {
            this.openPopup();
        });
@endif