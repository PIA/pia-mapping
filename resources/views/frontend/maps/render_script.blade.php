@php $map_slug = str_slug('m_'.$map->label.'_'.$map->id, '_') @endphp

<script>

    window.maps_holder = [];
    window.map = undefined;
    
    document.addEventListener('DOMContentLoaded', function () {
    
        L.Map.addInitHook(function () {
            maps_holder.push(this);
        });
    
        // L.PM.setOptIn(true);
    
        let center = [46.818188, 8.227512],
            layers = {}, keys = {};
    
        map = L.map('{{ $map_slug }}', {
            center: center,
            zoom: 8,
            doubleClickZoom: false
        });
    
        map.doubleClickZoom.disable(); 
    
        @include('frontend/maps/tiles', ['map' => $map])
    
        @foreach($layers as $layer)
    
            @php $layer_slug = str_slug('l_'.$layer->label.'_'.$layer->id, '_') @endphp
    
            let {{ $layer_slug }} = L.markerClusterGroup();
    
            layers['{{ $layer->label }}'] = {{ $layer_slug }};
    
            @foreach($layer->mapEntries as $map_entry)
                @php
                    $render = true;
                @endphp
                @if(isset($single_entry) && $map_entry->id == $single_entry->id)
                    @php
                        $render = false;
                    @endphp
                @endif
                @if($render)
                    @include('frontend/maps/entry', ['map_entry' => $map_entry])
                @endif
            @endforeach
    
            {{ $layer_slug }}.addTo(map);
    
        @endforeach
    
        L.control.layers(null, layers).addTo(map);
    
        if(Object.keys(keys).length > 0) {
            var legend = L.control({position: 'bottomleft'});
            legend.onAdd = function (map) {
    
                let legend_content = L.DomUtil.create('div', 'legend');
    
                legend_content.innerHTML = '<span class="icon">🗺️</span>';
    
                for (let key in keys) {
                    legend_content.innerHTML += '<label><input class="key" type="checkbox" value="'+key+'" checked/> '+keys[key].label+'</label><br>';
                }
    
                legend_content.querySelectorAll('input.key').forEach(el => {
                    el.addEventListener('change', evt => {
                        let entries = keys[evt.target.value].entries;
    
                        for(let e in entries) {
                            let entry = entries[e];
    
                            if(evt.target.checked){
                                layers[entry.layer].addLayer(entry);
                            } else {
                                layers[entry.layer].removeLayer(entry);
                            }
                        }
                        
                        for(let l in layers){
                            layers[l].refreshClusters();
                        }
                    })
                })
    
                return legend_content;
            };
            legend.addTo(map);
        }
        
    
    });
    
    </script>