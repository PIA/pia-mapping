let entry_{{ $editable_entry->id }} = new L.layerGroup();

map.pm.addControls({
    position: 'topleft',
    drawCircleMarker: false
});

map.pm.setGlobalOptions({
    layerGroup: entry_{{ $editable_entry->id }}
})

entry_{{ $editable_entry->id }}.addTo(map);

map.on('pm:create pm:globaleditmodetoggled pm:globaldragmodetoggled pm:globalremovalmodetoggled pm:globalcutmodetoggled pm:globalrotatemodetoggled', e => {
    let data = {
        "type":"FeatureCollection",
        "features": []
    };

    entry_{{ $editable_entry->id }}.getLayers().forEach(function(layer){
        let json = layer.toGeoJSON();

        if (layer instanceof L.Circle) {
            json.properties.radius = layer.getRadius();
        }

        data.features.push(json);
    });

    document.querySelector('[name="complex_data"]').value = JSON.stringify(data);
})

@if ($editable_entry->complex_data)
    let data = {!! $editable_entry->complex_data !!};
    data.features.forEach((el, i) => {
        L.geoJSON(el, {
            pointToLayer: (feature, latlng) => {
                if (feature.properties.radius) {
                    return new L.Circle(latlng, feature.properties.radius);
                } else {
                    return new L.Marker(latlng, {pmIgnore: false});
                }
            },
            onEachFeature: (feature, layer) => {
                layer.addTo(entry_{{ $editable_entry->id }});

                if (typeof layer.setStyle !== "undefined") { 
                    layer.setStyle({pmIgnore: false});
                    L.PM.reInitLayer(layer);  
                }
            },
        });
    })
@endif

function get_geojson(){
    let data = {
            "type":"FeatureCollection",
            "features": []
        },
        layers = find_layers(entry_{{ $editable_entry->id }});

        console.log(layers)

    layers.forEach(function(layer){
        let json = layer.toGeoJSON();

        if (layer instanceof L.Circle) {
            json.properties.radius = layer.getRadius();
        }

        data.features.push(json);
    });

    document.querySelector('[name="complex_data"]').value = JSON.stringify(data);
}

function find_layers(layergroup) {
    let layers = [];
    layergroup.getLayers().forEach(layer => {
        if (
            layer instanceof L.Polyline || //Don't worry about Polygon and Rectangle they are included in Polyline
            layer instanceof L.Marker ||
            layer instanceof L.Circle ||
            layer instanceof L.CircleMarker
        ) {
            layers.push(layer);
        }
    });

    // filter out layers that don't have the leaflet-geoman instance
    layers = layers.filter(layer => !!layer.pm);

    // filter out everything that's leaflet-geoman specific temporary stuff
    layers = layers.filter(layer => !layer._pmTempLayer);

    return layers;
}