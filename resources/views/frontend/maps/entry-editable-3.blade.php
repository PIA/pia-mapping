let image_input = document.querySelector('.pia-image-input'),
    image_label = document.querySelector('.pia-image-label'),
    image_overlay = undefined;

image_input.addEventListener('change', e => {

    if(map.hasLayer(image_overlay)) {
        map.removeLayer(image_overlay)
    }

    image_label.classList.remove('is-hidden');
    image_label.innerHTML = image_input.files[0].name;

    var reader = new FileReader();
    reader.onload = function (e) {
        var img = new Image;
        img.onload = function() {

            let image_url = 'data:'+reader.result;

            image_overlay = L.distortableImageOverlay(image_url, {
                mode: 'scale',
                actions: [L.DragAction, L.ScaleAction, L.DistortAction, L.RotateAction, L.FreeRotateAction, L.OpacityAction],
            }).addTo(map);
        };
        img.src = reader.result;
    };
    reader.readAsDataURL(image_input.files[0]);
})

@if ($editable_entry->image->file_name != '')
image_overlay = L.distortableImageOverlay(`/storage/uploads/{{$entry->image->file_name}}`, {
    mode: 'scale',
    actions: [L.DragAction, L.ScaleAction, L.DistortAction, L.RotateAction, L.FreeRotateAction, L.OpacityAction],
    corners: {!! $editable_entry->complex_data !!}
}).addTo(map);
@endif

document.querySelector('#map_entry_form').addEventListener('submit', e => {
    document.querySelector('[name="complex_data"]').value = JSON.stringify(image_overlay.getCorners());
})