@extends('frontend/base')

@section('content')
<x-header>
    <div class="flex flex-1 justify-end">

        <x-fe-button id="export-geojson" class="ml-4">GeoJSON</x-fe-button>
        <x-fe-a-button href="{{ route('maps.export', [$map]) }}" class="ml-2">Export</x-fe-a-button>
            
        <form method="POST" action="{{ route('maps.destroy', ['map' => $map->id]) }}" onsubmit="return confirm('Do you really want to submit the form?');">
            @csrf
            @method('delete')

            <button class="border border-red-500 bg-red-500 text-white hover:bg-white hover:text-red-500 p-1 px-6 transition uppercase text-xs ml-4" type="submit">Delete Map</button>
        </form>
    </div>
</x-header>

<div class="flex min-h-screen">
    <aside class="w-1/3 shadow-2xl z-40" style="padding-top: 45px;">
        <div>
            <div class="py-2 px-4 flex border-b border-black bg-gray-100 justify-between">
                <h2 class="text-xl font-bold">
                    <span>Map Content</span>
                </h2>
                <div class="flex">
                    <form id="import-map-entries" method="POST" enctype="multipart/form-data"  action="{{ route('map_entries.import') }}" >
                        @csrf
                        <input type="hidden" name="map_id" value="{{ $map->id }}">
                        <label class="text-xs underline py-1 pr-4 cursor-pointer">
                            Import
                            <input id="import-map-entries-list" class="hidden" type="file" name="file" accept=".csv" required>
                        </label>
                    </form>

                    <form method="POST" action="{{ route('map_layers.store') }}">
                        @csrf
                        <input type="hidden" name="map_id" value="{{ $map->id }}">
                        <input type="hidden" name="label" value="New Layer">
                        <x-fe-button class="text-xs" type="submit">+ Layer</x-fe-button>
                    </form>
                </div>
            </div>
            
            @foreach($map->mapLayers as $map_layer)
                <div>
                    <h3 class="flex text-lg py-2 px-4 justify-between">
                        <a class="hover:underline" href="{{ route('map_layers.show', ['map_layer' => $map_layer->id]) }}">{{ $map_layer->label }}</a>
                        <a class="text-xs underline py-1 pr-4 cursor-pointer" href="{{ route('map_entries.batch', ['map_layer_id' => $map_layer->id]) }}">Batch</a>
                    </h3>
                    <div x-data="{ show_all: false }">
                        @forelse($map_layer->mapEntries->sortBy('label', SORT_NATURAL , false) as $entry)
                            <a href="{{ route('map_entries.show', ['map_entry' => $entry->id]) }}" class="map-entry py-2 px-4 text-sm inline-block w-full hover:underline"
                                {{ $loop->index >= 3 ? 'x-show=show_all' : '' }}>

                                <div class="inline-block relative" style="width: 18px; height: 23px; top: 6px;">
                                    @foreach ($entry->mapKeys as $key)
                                        @if($key->icon_file_name)
                                            <img src="/storage/legend-icons/{{ $key->icon_file_name }}" class="inline-block absolute top-0 left-0" style="width: 14px;">
                                        @endif
                                        @if($key->icon)
                                            <span class="inline-block absolute left-0" style="top: 3px;">{{ $key->icon }}</span>
                                        @endif
                                    @endforeach
                                </div>
                                
                                {{ $entry->label }}
                            </a>
                            @if ($loop->index == 3)
                            <a href="javascript:;" class="py-2 px-4 border-b text-xs inline-block w-full hover:bg-black hover:text-white"
                                @click.prevent="show_all = ! show_all" x-show="! show_all">… show more</a>
                            @endif
                        @empty
                            <div class="pb-2"></div>
                        @endforelse
                    </div>
                    <div class="flex px-4 py-2 border-b text-xs justify-end border-gray-300">
                        <span class="mr-2">Add</span>
                        <form method="POST" action="{{ route('map_entries.store') }}">
                            @csrf
                            <input type="hidden" name="label" value="New Marker">
                            <input type="hidden" name="map_id" value="{{ $map->id }}">
                            <input type="hidden" name="layer_id" value="{{ $map_layer->id }}">
                            <input type="hidden" name="type" value="1">

                            <button type="submit" class="mr-2 underline">Marker</button>
                        </form>

                        <form method="POST" action="{{ route('map_entries.store') }}">
                            @csrf
                            <input type="hidden" name="label" value="New Drawing">
                            <input type="hidden" name="map_id" value="{{ $map->id }}">
                            <input type="hidden" name="layer_id" value="{{ $map_layer->id }}">
                            <input type="hidden" name="type" value="2">

                            <button type="submit" class="mr-2 underline">Drawing</button>
                        </form>

                        <form method="POST" action="{{ route('map_entries.store') }}">
                            @csrf
                            <input type="hidden" name="label" value="New Image">
                            <input type="hidden" name="map_id" value="{{ $map->id }}">
                            <input type="hidden" name="layer_id" value="{{ $map_layer->id }}">
                            <input type="hidden" name="type" value="4">

                            <button type="submit" class="underline mr-2">Image</button>
                        </form>

                        <form method="POST" action="{{ route('map_entries.store') }}">
                            @csrf
                            <input type="hidden" name="label" value="New Overlay">
                            <input type="hidden" name="map_id" value="{{ $map->id }}">
                            <input type="hidden" name="layer_id" value="{{ $map_layer->id }}">
                            <input type="hidden" name="type" value="3">

                            <button type="submit" class="underline">Overlay</button>
                        </form>
                    </div>
                </div>
            @endforeach

            @if (count($maps) > 1)
                <div>
                    <h3 class="flex justify-between text-lg py-2 px-4 border-t border-gray-500">
                        <span>Map Linking</span>
                    </h3>
                    <form method="POST" action="{{ route('maps.link', [$map]) }}">
                        @csrf
                        <select id="map-layers" class="text-sm inline-block w-full py-2 px-4 " name="links[]" id="links" multiple size="5">
                            @foreach($maps as $_map)
                                @if($map->id != $_map->id)
                                <optgroup class="py-2" label="{{ $_map->label }}" class="py-2 px-4 font-normal">
                                    @foreach($_map->mapLayers as $map_layer)
                                        <option class="py-2 px-4" value="{{ $map_layer->id }}" {{ $map->linkedLayers->contains($map_layer) ? 'selected="selected"' : '' }}>{{ $map_layer->label }}</option>
                                    @endforeach
                                </optgroup>
                                @endif
                            @endforeach 
                        </select>
                        <div class="px-4 pb-2 flex justify-end">
                            <x-fe-button>Link Selected layers</x-fe-button>
                        </div>
                    </form>
                </div>
            @endif

            <div class="py-2 px-4 flex border-b border-t border-black bg-gray-100 justify-between">
                <h2 class="text-xl font-bold">
                    <span>Map Documents</span>
                </h2>
            </div>
            <div class="py-2 px-4">
                @if(count($map->documents))
                <ul class="py-2">
                    @foreach ($map->documents as $document)
                    <li class="mb-2">
                        &mdash; <a class="underline text-sm" href="/{{ 'storage/' . $document->base_path . '/' . $document->file_name }}">{{ $document->label }}</a>
                    </li>
                    @endforeach
                </ul>
                @endif
                <form class="flex justify-between inline-block" method="POST" enctype="multipart/form-data"
                    action="{{ route('maps.uploadDocuments', [$map]) }}">
                    @csrf
                    <input type="file" name="documents[]" required multiple>
                    <x-fe-button>Upload</x-fe-button>
                </form>
            </div>


            <div>
                <h2 class="py-2 px-4 text-xl border-b border-t border-black bg-gray-100 font-bold">Map Configuration</h2>

                <div>
                    <form method="POST" action="{{ route('maps.update', ['map' => $map->id]) }}" class="flex justify-between align-middle">
                        @csrf
                        @method('patch')

                        <input type="hidden" name="tiles" value="{{ $map->tiles }}">
                        
                        <input class="w-full py-2 px-4 text-lg focus:outline-none" type="text" name="label" placeholder="Label" value="{{ $map->label }}" required>
                        <div class="mt-2 mr-4">
                            <x-fe-button>Change&nbsp;Title</x-fe-button>
                        </div>
                        <!--<textarea class="w-full py-2 px-4" name="description" placeholder="Beschreibung">{{ $map->description }}</textarea>-->
                    </form>
                </div>

                <div>
                    
                    <div class="flex justify-between border-t border-gray-500 py-2 px-4">
                        <h3 class="text-lg">
                            <span>Legend</span>
                        </h3>
                        <div class="flex">
                            

                            <form id="import-map-key-entries" method="POST" enctype="multipart/form-data" action="{{ route('map_keys.import') }}" >
                                @csrf

                                <a class="text-xs underline py-1 pr-4 cursor-pointer" href="{{ route('map_keys.batch', ['map_id' => $map->id]) }}">Batch</a>

                                <input type="hidden" name="map_id" value="{{ $map->id }}">
                                <label class="text-xs underline py-1 pr-4 cursor-pointer">
                                    Import
                                    <input id="import-map-key-entries-list" class="hidden" type="file" name="file" accept=".csv" required>
                                </label>
                            </form>

                            <form method="POST" action="{{ route('map_keys.store') }}">
                                @csrf
                                <input type="hidden" name="map_id" value="{{ $map->id }}">
                                <input type="hidden" name="label" value="New Map-Key">
                                <x-fe-button class="text-xs" type="submit">+ Key</x-fe-button>
                            </form>
                        </div>
                    </div>

                    <div x-data="{ show_all: false }">
                        @forelse($map->mapKeys as $key)
                            <a href="{{ route('map_keys.show', ['map_key' => $key->id]) }}" class="py-2 px-4 border-t border-gray-300 text-sm inline-block w-full hover:underline"
                                {{ $loop->index >= 3 ? 'x-show=show_all' : '' }}>
                                @if($key->icon_file_name)<img src="/storage/legend-icons/{{ $key->icon_file_name }}" class="inline-block" style="width: 14px; position: relative; top: -3px;">@elseif($key->icon){{ $key->icon }} @else &mdash; @endif {{ $key->label }}
                            </a>
                            @if ($loop->index == 3)
                            <a href="javascript:;" class="py-2 px-4 border-t border-gray-300 text-xs inline-block w-full hover:bg-black hover:text-white"
                                @click.prevent="show_all = ! show_all" x-show="! show_all">… show more</a>
                            @endif
                        @empty
                            <div></div>
                        @endforelse
                    </div>
                </div>
                <div x-data>
                    <form method="POST" action="{{ route('maps.update', ['map' => $map->id]) }}" x-ref="form">
                        @csrf
                        @method('patch')

                        <input type="hidden" name="label" value="{{ $map->label }}">
                        <input type="hidden" name="description" value="{{ $map->description }}">
                        <div class="mb-4 py-2 px-4 border-t border-b border-gray-500">
                            <select id="tiles" name="tiles" id="tiles"  class="text-lg w-full" @change="$refs.form.submit()">
                                <option value="1" {{ $map->tiles == 1 ? 'selected="selected"' : '' }}>Mapbox Light</option>
                                <option value="2" {{ $map->tiles == 2 ? 'selected="selected"' : '' }}>OpenTopo</option>
                                <option value="3" {{ $map->tiles == 3 ? 'selected="selected"' : '' }}>Stamen Toner Lite</option>
                                <option value="4" {{ $map->tiles == 4 ? 'selected="selected"' : '' }}>Stamen Watercolor</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </aside>
        
    <div class="flex-1 h-full w-2/3 fixed right-0" style="padding-top: 45px;">
        @include('frontend/maps/render', ['map' => $map])
    </div>
</div>
@endsection

@section('scripts')

    <script src="/node_modules/file-saver/dist/FileSaver.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', () => {

            @if (count($maps) > 1)
            new SlimSelect({
                select: '#map-layers',
                placeholder: 'Select Layers'
            });
            @endif

            document.querySelector('#export-geojson').addEventListener('click', evt => {
                let map = maps_holder[0],
                    data = {
                        "type":"FeatureCollection",
                        "features": []
                    };

                map.eachLayer(layer => {
                    if (layer instanceof L.LayerGroup) {
                        layer.toGeoJSON().features.forEach((el) => {
                            data.features.push(el);
                        })
                    }
                });

                var blob = new Blob([JSON.stringify(data)], {type: "application/geo+json;charset=utf-8"});
                saveAs(blob, '{{ str_slug($map->label, "_") }}.geojson');
            })

            document.querySelector('#import-map-entries-list').addEventListener('change', evt => {
                document.querySelector('#import-map-entries').submit();
            });

            document.querySelector('#import-map-key-entries-list').addEventListener('change', evt => {
                document.querySelector('#import-map-key-entries').submit();
            });
        })
    </script>

@endsection
