@extends('frontend/base')

@section('content')

<x-header>
    <div class="flex flex-1 justify-end">
        <a href="{{ route('maps.show', ['map' => $editable_entry->mapLayer->map->id]) }}" class="ml-2 hover:underline">Map</a>
    </div>
</x-header>

<div class="flex min-h-screen">
    <aside class="w-1/3 shadow-2xl z-40" style="padding-top: 45px;">
        <div>
            <form id="map_entry_form" method="POST" enctype="multipart/form-data" action="{{ route('map_entries.update', ['map_entry' => $editable_entry->id]) }}">
                @csrf
                @method('patch')
                <input type="hidden" name="map_id" value="{{ $editable_entry->mapLayer->map->id }}">
                <input type="hidden" name="layer_id" value="{{ $editable_entry->mapLayer->id }}">
                
                @if (in_array($editable_entry->type, [2,3]))
                <input type="hidden" name="complex_data" value="{{ $editable_entry->complex_data }}">
                @endif

                <input class="py-2 px-4 text-xl border-b border-black bg-gray-100 flex justify-between font-bold w-full" type="text" name="label" placeholder="Label" value="{{ $editable_entry->label }}" required>
                <!--<textarea class="textarea" name="description" placeholder="Beschreibung">{{ $editable_entry->description }}</textarea>-->

                @if (in_array($editable_entry->type, [1, 4]))
                <div class="flex">
                    <input type="text" class="py-2 px-4 focus:outline-none w-1/2 text-xs border-b border-black bg-gray" name="latitude" value="{{ $editable_entry->place->latitude ?? '' }}" placeholder="Latitude">
                    <input type="text" class="py-2 px-4 focus:outline-none w-1/2 text-xs border-b border-black bg-gray" name="longitude" value="{{ $editable_entry->place->longitude ?? '' }}" placeholder="Longitude">
                </div>
                @endif

                @if (in_array($editable_entry->type, [3, 4]))
                    <div x-data class="py-2 px-4 w-full justify-between border-b border-gray-400">
                        <span class="file-name {{ $editable_entry->image->original_file_name == '' ? 'is-hidden' : '' }} pia-image-label mb-2 w-full inline-block">
                            {{$editable_entry->image->original_file_name}}
                        </span>
                        <x-fe-button type="button" @click="$refs.file.click()">Choose image file</x-fe-button>
                        <input x-ref="file" class="file-input pia-image-input hidden" type="file" name="image" accept="image/*">
                    </div>
                @endif

                <h2 class="py-2 px-4 text-md">Choose/Change existing place</h2>
                <select id="places" class="text-sm inline-block w-full py-2 px-4" name="place_id">
                    <option value="">&mdash;</option>
                    @foreach($places as $place)
                        <option value="{{ $place->id }}" {{ ($editable_entry->place->id ?? -1) == $place->id ? 'selected="selected"' : '' }}>{{ $place->label }}</option>
                    @endforeach
                </select>
                @if($editable_entry->place)
                <p class="px-4 text-right"><a class="text-xs underline" href="https://pia.dhlab.unibas.ch/places/{{ $editable_entry->place->id }}">View/Edit place details</a></p>
                @endif

                @if (count($keys))
                <h2 class="py-2 px-4 text-lg">
                    Legend Keys
                </h2>
                <select id="map-keys" class="text-sm inline-block w-full py-2 px-4 " name="keys[]" multiple size="10">
                </select>
                @endif

                <div class="text-right mt-4 px-4 flex justify-between">
                    <a href="{{ route('maps.show', ['map' => $editable_entry->mapLayer->map->id]) }}" class="hover:underline">Back</a>
                    <x-fe-button>Save</x-fe-button>
                </div>

            </form>
        </div>

        <div class="p-4">
            <h2 class="text-lg">
                Documents
            </h2>
            @if(count($editable_entry->documents))
            <ul class="py-2">
                @foreach ($editable_entry->documents as $document)
                <li class="mb-2">
                    &mdash; <a class="underline text-sm" href="/{{ 'storage/' . $document->base_path . '/' . $document->file_name }}">{{ $document->label }}</a>
                </li>
                @endforeach
            </ul>
            @endif
            <form class="flex justify-between inline-block" method="POST" enctype="multipart/form-data"
                action="{{ route('map_entries.uploadDocuments', [$editable_entry]) }}">
                @csrf
                <input type="file" name="documents[]" required multiple>
                <x-fe-button>Upload</x-fe-button>
            </form>
        </div>

        <form method="POST" action="{{ route('map_entries.destroy', ['map_entry' => $editable_entry->id]) }}" onsubmit="return confirm('Do you really want to delete this entry?');">
            @csrf
            @method('delete')
            <input type="hidden" name="map_id" value="{{ $editable_entry->mapLayer->map->id }}">
            <button class="absolute bottom-4 border border-red-500 bg-red-500 text-white hover:bg-white hover:text-red-500 p-1 px-6 transition uppercase text-xs ml-4" type="submit">Delete Entry</button>
        </form>
    </aside>

    <div class="flex-1 h-full w-2/3 fixed right-0" style="padding-top: 45px;">
        @include('frontend/maps/render', [
            'map' => $editable_entry->mapLayer->map,
            'layers' => $editable_entry->mapLayer->map->mapLayers
        ])
    </div>
</div>
@endsection

@section('scripts')
<script>

    document.addEventListener('DOMContentLoaded', () => {
        @if (count($keys))
        new SlimSelect({
            select: '#map-keys',
            placeholder: 'Select Legend Keys',
            data: [
                @foreach($keys as $key)
                    {
                        innerHTML: '@if($key->icon_file_name)<img src="/storage/legend-icons/{{ $key->icon_file_name }}" class="inline-block" style="height: 18px; position: relative; top: -2px;">@endif {{ $key->icon }} {{ $key->label }}',
                        text: '{{ $key->label }}',
                        value: '{{ $key->id }}',
                        selected: {{ $editable_entry->mapKeys->contains($key) ? 'true' : 'false' }}
                    },
                @endforeach
            ]
        });
        @endif

        new SlimSelect({
            select: '#places',
        });
    });
    
</script>
@endsection
