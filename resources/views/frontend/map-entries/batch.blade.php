@extends('frontend/base')

@section('content')

<x-header>
    <div class="flex flex-1 justify-end">
        <a href="{{ route('maps.show', ['map' => $layer->map->id]) }}" class="ml-2 hover:underline">Map</a>
    </div>
</x-header>

<div class="flex min-h-screen">
    <aside class="w-1/3 shadow-2xl z-40" style="padding-top: 45px;">
        <div>
            <form id="map_entry_form" method="POST" action="{{ route('map_entries.batch_update') }}">
                @csrf

                <input type="hidden" name="map_layer_id" value="{{ $layer->id }}">

                @foreach ($entries->sortBy('label', SORT_NATURAL , false) as $entry)
                    <div class="py-2 px-4">
                        <input type="hidden" name="map-entry-{{ $entry->id }}" value="{{ $entry->id }}">
                        <input type="text" name="label-{{ $entry->id }}" value="{{ $entry->label }}" class="w-full"/>
                        <select id="map-keys-{{ $entry->id }}" class="text-sm inline-block w-full mt-1" name="keys-{{ $entry->id }}[]" multiple size="10">
                            
                        </select>
                    </div>
                @endforeach
                
                <div class="text-right mt-4 px-4 flex justify-between">
                    <a href="{{ route('maps.show', ['map' => $layer->map->id]) }}" class="hover:underline">Back</a>
                    <x-fe-button>Save</x-fe-button>
                </div>
            </form>
        </div>
    </aside>

    <div class="flex-1 h-full w-2/3 fixed right-0" style="padding-top: 45px;">
        @include('frontend/maps/render', [
            'map' => $layer->map,
            'layers' => $layer->map->mapLayers
        ])
    </div>
</div>
@endsection

@section('scripts')
<script>

    document.addEventListener('DOMContentLoaded', () => {
        @foreach ($entries as $entry)
            new SlimSelect({
                select: '#map-keys-{{ $entry->id }}',
                placeholder: 'Select Legend Keys',
                data: [
                    @foreach($keys as $key)
                        {
                            innerHTML: '@if($key->icon_file_name)<img src="/storage/legend-icons/{{ $key->icon_file_name }}" class="inline-block" style="height: 18px; position: relative; top: -2px;">@endif {{ $key->icon }} {{ $key->label }}',
                            text: '{{ $key->label }}',
                            value: '{{ $key->id }}',
                            selected: {{ $entry->mapKeys->contains($key) ? 'true' : 'false' }}
                        },
                    @endforeach
                ]
            });
        @endforeach
    });
    
</script>
@endsection